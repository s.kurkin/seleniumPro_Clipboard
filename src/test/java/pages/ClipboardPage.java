package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClipboardPage extends BasePage {

    @FindBy(xpath = "//div[@id = 'example-text']/button")
    private WebElement copyToClipboardBtn;

    @FindBy(xpath = "//span[contains(text(),'Just')]")
    private WebElement codeAreaElement;

    public ClipboardPage(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void copyBtnClick() {
        clickElement(copyToClipboardBtn);
    }

    public String getCodeAreaText() {
        return getElement(codeAreaElement).getText();
    }
}
