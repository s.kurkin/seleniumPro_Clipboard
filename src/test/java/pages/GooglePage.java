package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class GooglePage extends BasePage {

    @FindBy(xpath = "//input[@id = 'lst-ib']")
    private WebElement searchBoxInput;

    public GooglePage(RemoteWebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchBox() {
        return getElement(searchBoxInput);
    }
}
