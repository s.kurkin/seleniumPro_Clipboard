package cases;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ClipboardPage;
import pages.GooglePage;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;

public class ClipboardCase {

    private final String CLIPBOARD_PAGE_URL = "https://clipboardjs.com/";
    private final String GOOGLE_PAGE_URL = "https://google.com/";
    private String searchText;
    private RemoteWebDriver driver;
    private ClipboardPage clipboardPage;
    private GooglePage googlePage;

    @BeforeClass
    public void setUp() throws Exception {
        ChromeDriverManager.getInstance().version("2.29").setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testClipboard() throws IOException {
        driver.get(CLIPBOARD_PAGE_URL);
        clipboardPage = new ClipboardPage(driver);
        clipboardPage.copyBtnClick();
        String clipboardContent = getClipboardContent();
        System.out.println("Clipboard content - " + clipboardContent);
        String codeAreaContent = clipboardPage.getCodeAreaText();
        System.out.println("Code area content - " + codeAreaContent);
        Assert.assertNotEquals(codeAreaContent, clipboardContent);

        driver.get(GOOGLE_PAGE_URL);
        googlePage = new GooglePage(driver);
        setFocusOnSearchInput();
        setClipboardContent();
        googlePage.getSearchBox().sendKeys(Keys.CONTROL + "v");
    }

    private String getClipboardContent() {
        String result = "";
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable contents = clipboard.getContents(null);
        boolean hasText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
        if (hasText) {
            try {
                result = (String) contents.getTransferData(DataFlavor.stringFlavor);
            } catch (UnsupportedFlavorException | IOException ex) {
                fail("Clipboard hasn't text");
            }
        }
        return result;
    }

    private void setClipboardContent() {
        StringSelection stringSelection = new StringSelection(searchText);
        Toolkit.getDefaultToolkit().getSystemClipboard()
                .setContents(stringSelection, null);
    }

    private void setFocusOnSearchInput() {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("return document.getElementById('lst-ib').focus");
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            searchText = config.getProperty("google.searchbox.text");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.close();
        driver.quit();
    }
}
